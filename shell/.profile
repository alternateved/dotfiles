#!/bin/sh

### Path
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
export PATH="$HOME"/.local/bin:"$PATH"
export PATH="$HOME"/.cabal/bin:"$PATH"
export PATH="$HOME"/.ghcup/bin:"$PATH"
export PATH="$HOME"/.cargo/bin:"$PATH"
export PATH="$HOME"/.gem/bin:"$PATH"
export PATH="$HOME"/.go/bin:"$PATH"
export PATH="$HOME"/.scripts:"$PATH"

### Variables
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_DATA_HOME="$HOME"/.local/share
export XDG_STATE_HOME="$HOME"/.local/state
export XDG_CACHE_HOME="$HOME"/.cache
export GEM_HOME="$HOME"/.gem
export GOPATH="$HOME"/.go

export ALTERNATE_EDITOR=
export LANG="en_US.UTF-8"
export EDITOR="emacsclient -t"
export BROWSER="firefox"
export TERMINAL="foot"
