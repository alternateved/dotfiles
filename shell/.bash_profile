#!/bin/env sh

### Load profile
[ -f "$HOME"/.profile ] && . "$HOME"/.profile

### Load configuration
[ -f "$HOME"/.bashrc ] && . "$HOME"/.bashrc

### Set SSH_AUTH_SOCK if not set already
[ -z "$SSH_AUTH_SOCK" ] && export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR"/keyring/ssh
