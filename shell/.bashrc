#!/bin/bash

### Test for an interactive shell
if [[ $- != *i* ]]; then
    return
fi

### Source global definitions
[ -f /etc/bashrc ] && . /etc/bashrc
[ -f /etc/bash.bashrc ] && . /etc/bash.bashrc

### History settings
HISTCONTROL=ignoreboth:erasedups # erase duplicates
HISTIGNORE="?:??"                # ignore one and two letter commands
HISTSIZE=500000                  # amount of history to save
HISTFILESIZE=100000              # max size of history file

### Options
shopt -s histappend          # append to history
shopt -s checkwinsize        # update lines and columns if window size has changed
shopt -s cdspell dirspell    # correct cd errors and directory names
shopt -s globstar nocaseglob # enable case-insensitive ** recursive globbing
shopt -s cmdhist             # save multi-line commands as one command

### Aliases
alias rm='rm -i'
alias cp='cp -i'
alias df='df -h'
alias free='free -m'
alias grep='grep --color=auto'
alias h='history'
alias j="jobs"
alias q='exit'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

if command -v rg > /dev/null; then
    alias rg='rg --smart-case'
    alias hg='history | rg'
else
    alias hg='history | grep'
fi

if command -v eza > /dev/null; then
    alias eza='eza --group-directories-first'
    alias ls='eza'
    alias l='eza -a'
    alias la='eza -laF'
    alias ll='eza -lF'
    alias tree='eza --tree'
else
    alias ls='ls --color=auto --group-directories-first'
    alias l='ls -A'
    alias la='ls -lAF'
    alias ll='ls -lF'
fi

if command -v emacs > /dev/null; then
    alias e='emacs -nw'
    alias em='emacsclient -t'
    alias dired="emacsclient -t --eval '(dired default-directory)'"
    alias magit='emacsclient -t --eval "(progn (magit-status) (delete-other-windows))"'
fi

if command -v toolbox > /dev/null; then
    alias te='toolbox enter'
    alias tr='toolbox run'
    alias tem='toolbox run emacsclient -t'
fi

### Terminal support
export TERM=xterm-ghostty

# Helper function for commands that require legacy terminal
legacy() { TERM=xterm-256color "$@"; }

# Disable flow control
stty -ixon

### Prompt
is_ssh() {
    [ -n "$SSH_CONNECTION" ] && printf "\[\e[0;1;90m\]\h\[\e[0m\]: "
}

is_container() {
    [ -f /run/.containerenv ] || [ -f /.dockerenv ] && printf "\[\e[35m\]⬢\[\e[0m\] "
}

PROMPT_DIRTRIM=3
PS1="$(is_container)$(is_ssh)\[\e[0;94m\]\w \[\e[0m\]\$ \[\e[0m\]"

### Foot integration
if [ "$TERM" = foot-direct ]; then
    mark_command_start() { printf '\e]133;C\e\\'; }
    mark_command_end() { printf '\e]133;D\e\\'; }
    mark_prompt() { printf '\e]133;A\e\\'; }
    mark_title() { test ! -n "$BASH_COMMAND" || echo -en "\033]0;../$(basename "${PWD}")\007"; }

    mark_directory() {
        local strlen=${#PWD}
        local encoded=""
        local pos c o
        for ((pos = 0; pos < strlen; pos++)); do
            c=${PWD:$pos:1}
            case "$c" in
                [-/:_.!\'\(\)~[:alnum:]])  o="${c}" ;;
                *)  printf -v o '%%%02X' "'${c}" ;;
            esac
            encoded+="${o}"
        done
        printf '\e]7;file://%s%s\e\\' "${HOSTNAME}" "${encoded}"
    }

    markers() {
        mark_title
        mark_command_end
        mark_directory
        mark_prompt
    }

    unset PROMPT_COMMAND
    PS0+="$(mark_command_start)"
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }markers
    trap 'echo -ne "\e]0;${BASH_COMMAND}\007"' DEBUG
fi

### Emacs integration
case "$INSIDE_EMACS" in
    *comint) PS1="\w \$ " ;;
    *vterm) [ -n "$EMACS_VTERM_PATH" ] && source "$EMACS_VTERM_PATH"/etc/emacs-vterm-bash.sh ;;
esac
