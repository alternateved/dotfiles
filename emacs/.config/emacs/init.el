;;; init.el --- Init File -*- lexical-binding: t -*-

;;; Commentary:

;; This is my personal configuration. It is slowly growing me.

;; In my configuration I use conventions inspired by Doom Emacs:
;; + `+NAME' denotes a command designed to be used interactively
;; + `+NAME-h' a non-interactive function meant to be used exclusively as a hook
;; + `+NAME-a' functions designed to be used as advice for other functions
;; + `+NAME-fn' helpers used for fetching data or augmenting other actions

;;; Code:

;;; Initialisation

;;;; Setup package.el

(require 'package)
(setopt package-archives
        '(("gnu"    . "https://elpa.gnu.org/packages/")
          ("nongnu" . "https://elpa.nongnu.org/nongnu/")
          ("melpa"  . "https://melpa.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(setopt package-native-compile t             ; natively compile packages during installation
        package-install-upgrade-built-in t   ; upgrade built-in packages such as transient or eglot
        package-vc-register-as-project nil)  ; I would prefer to decide that manually

;;;; Configure use-package

(use-package use-package
  :custom
  (use-package-expand-minimally t)
  (use-package-always-ensure t)
  (use-package-always-defer t)
  (use-package-vc-prefer-newest t)
  (use-package-enable-imenu-support t))

;;;; Load other files

(add-to-list 'load-path (locate-user-emacs-file "lisp/"))

;;;; Enable and disable commands

(dolist (cmd '(upcase-region
               downcase-region
               narrow-to-region))
  (put cmd 'disabled nil))
(put 'suspend-frame 'disabled t)

;;; Authentication

;;;; Personal information

(setopt user-full-name "Tomasz Hołubowicz"
        user-mail-address "mail@alternateved.com")

;;;; Authentication source

(use-package auth-source
  :ensure nil
  :custom
  (auth-source-cache-expiry nil)
  (password-cache-expiry nil)
  :config
  (defun +auth-get-field-fn (host prop)
    "Find PROP in `auth-sources' for HOST entry."
    (when-let* ((source (auth-source-search :host host)))
      (if (eq prop :secret)
          (funcall (plist-get (car source) prop))
        (plist-get (flatten-list source) prop)))))

;;; Files and projects

;;;; Options

(setopt auto-save-file-name-transforms
        `((".*" ,auto-save-list-file-prefix t))
        auto-save-include-big-deletions t
        make-backup-files nil
        create-lockfiles nil)

;; Load variables configured via the interactive 'customize' interface
(setopt custom-file (concat user-emacs-directory "custom.el"))

(when (file-exists-p custom-file)
  (load custom-file))

;;;; Recent files

(use-package recentf
  :ensure nil
  :hook
  (after-init . recentf-mode)
  (kill-emacs . recentf-cleanup)
  :bind ("C-x C-r" . recentf-open-files)
  :custom
  (recentf-max-saved-items 200)
  (recentf-auto-cleanup (if (daemonp) 300)))

;;;; Files

(use-package files
  :ensure nil
  :bind ("C-x /" . pwd)
  :custom
  (require-final-newline t)
  (trusted-content `(,user-emacs-directory)))

;;;; Dired

(use-package dired
  :ensure nil
  :hook (dired-mode . hl-line-mode)
  :bind (:map dired-mode-map ("C-<return>" . dired-do-open))
  :custom
  (delete-by-moving-to-trash t)
  (dired-listing-switches "-alGh --group-directories-first")
  (dired-kill-when-opening-new-dired-buffer t)
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)
  (dired-movement-style 'bounded)
  (dired-dwim-target t))

;;;; Project

(use-package project
  :ensure nil
  :custom
  (project-switch-commands
   '((project-find-file "Find file" ?f)
     (project-find-dir "Find directory" ?d)
     (consult-ripgrep "Find regexp" ?g)
     (consult-project-buffer "Find buffer" ?b)
     (project-compile "Compile" ?c)
     (magit-project-status "Magit" ?m)
     (project-eshell "Eshell" ?e)
     (+vterm-project "VTerm" ?t)))
  (project-vc-extra-root-markers '(".project")))

;;; History and state

;;;; Options

(setopt undo-limit 80000000
        history-delete-duplicates t
        kill-do-not-save-duplicates t)

;;;; Savehist

(use-package savehist
  :ensure nil
  :defer 1
  :hook (after-init . savehist-mode)
  :custom
  (savehist-save-minibuffer-history t)
  (savehist-additional-variables
   '(kill-ring
     register-alist
     last-kbd-macro kmacro-ring
     mark-ring global-mark-ring
     search-ring regexp-search-ring)))

;;;; Saveplace

;; Remember where the point is in any given file.
(use-package saveplace
  :ensure nil
  :defer 1
  :hook (after-init . save-place-mode)
  :custom (save-place-forget-unreadable-files t))

;;; Help system

;;;; Options

(setopt help-window-select t)

;;;; Eldoc

(use-package eldoc
  :ensure nil
  :defer 1
  :hook (after-init . global-eldoc-mode)
  :custom
  (eldoc-idle-delay 0.1)
  (eldoc-echo-area-use-multiline-p nil)
  (eldoc-echo-area-prefer-doc-buffer t)
  (eldoc-echo-area-display-truncation-message nil)
  (eldoc-documentation-function 'eldoc-documentation-compose))

;;; Keybindings

;;;; Definitions

;; Suppress "ad-handle-definition: x got redefined" warnings
(setopt ad-redefinition-action 'accept)

(define-minor-mode +bar-cursor-mode
  "Change cursor type into bar in current buffer."
  :init-value nil
  :global nil
  (if +bar-cursor-mode
      (setq-local cursor-type 'bar)
    (setq-local cursor-type 'box)
    (kill-local-variable 'cursor-type)))

(defun +open-emacs-configuration ()
  "Open Emacs configuration."
  (interactive)
  (find-file user-init-file))

;;;; Bindings

;; This binding conflicts with opening links in terminal
(keymap-global-unset "C-<down-mouse-1>")

;; A counter-measure for corrupted display
(keymap-global-set "<f5>" 'redraw-display)

;;;; Keymaps

(bind-keys :prefix-map toggle-map
           :prefix "C-c t"
           :prefix-docstring "Keymap for commands that toggle settings."
           ("b" . toggle-frame-tab-bar)
           ("c" . +bar-cursor-mode)
           ("d" . toggle-debug-on-error)
           ("n" . display-line-numbers-mode)
           ("v" . variable-pitch-mode)
           ("w" . whitespace-mode))

(bind-keys :prefix-map open-map
           :prefix "C-c o"
           :prefix-docstring "Keymap for opening places or applications."
           ("c" . +open-emacs-configuration)
           ("s" . scratch-buffer))

(bind-keys :prefix-map code-map
           :prefix "C-c c"
           :prefix-docstring "Keymap for different programming utilities."
           ("w" . whitespace-cleanup)
           ("d" . xref-find-definitions)
           ("D" . xref-find-definitions-other-window)
           ("r" . xref-find-references))

;;;; Repeatable key chords

;; Once ~repeat-mode~ is enabled, a key binding or chord that invokes a command
;; successfully can be repeated by typing in its tail or whatever the developer
;; specifies.
(use-package repeat
  :ensure nil
  :defer 1
  :hook (after-init . repeat-mode)
  :bind ("C-z" . repeat))

;;; Appearance

;;;; Vertical separators

(set-display-table-slot standard-display-table 'vertical-border (make-glyph-code ?│))

;;;; Mode-line

(defface +mode-line-gray
  '((t (:inherit (shadow))))
  "Face used for less important mode-line elements."
  :group '+mode-line-faces)

(defface +mode-line-indicator-blue
  '((((class color) (min-colors 88) (background light))
     :background "#0000aa" :foreground "white")
    (((class color) (min-colors 88) (background dark))
     :background "#77aaff" :foreground "black")
    (t :background "blue" :foreground "black"))
  "Face for modeline indicators with a background."
  :group '+mode-line-faces)

(defface +mode-line-indicator-cyan
  '((((class color) (min-colors 88) (background light))
     :background "#006080" :foreground "white")
    (((class color) (min-colors 88) (background dark))
     :background "#40c0e0" :foreground "black")
    (t :background "cyan" :foreground "black"))
  "Face for modeline indicators with a background."
  :group '+mode-line-faces)

(defface +mode-line-indicator-magenta
  '((((class color) (min-colors 88) (background light))
     :background "#6f0f9f" :foreground "white")
    (((class color) (min-colors 88) (background dark))
     :background "#e3a2ff" :foreground "black")
    (t :background "magenta" :foreground "black"))
  "Face for modeline indicators with a background."
  :group '+mode-line-faces)

(defvar-local +mode-line-narrow
    '(:eval
      (when (and (mode-line-window-selected-p)
                 (buffer-narrowed-p)
                 (not (derived-mode-p 'Info-mode 'help-mode 'special-mode 'message-mode)))
        (propertize " NARROW "
                    'face '+mode-line-indicator-cyan
                    'mouse-face 'mode-line-highlight
                    'help-echo "mouse-1: Remove narrowing from buffer"
                    'local-map (make-mode-line-mouse-map 'mouse-1 'mode-line-widen))))
  "Mode line construct for indicating narrowing.")

(put '+mode-line-narrow 'risky-local-variable t)

(defvar-local +mode-line-kmacro
    '(:eval
      (when (and (mode-line-window-selected-p)
                 (or defining-kbd-macro executing-kbd-macro))
        (propertize (format " MACRO (%d) " kmacro-counter)
                    'face '+mode-line-indicator-blue)))
  "Mode line construct for displaying macro being recorded.")

(put '+mode-line-kmacro 'risky-local-variable t)

(defvar-local +mode-line-position
    '(:eval (concat "  %l:%C" (propertize " %p  " 'face '+mode-line-gray)))
  "Mode line construct for displaying current cursor position.")

(put '+mode-line-position 'risky-local-variable t)

(defvar-local +mode-line-major-mode
    '(:eval
      (propertize (format-mode-line mode-name)
                  'face 'mode-line-buffer-id
                  'mouse-face 'mode-line-highlight
                  'local-map mode-line-major-mode-keymap
                  'help-echo (format
                              "Active minor modes: (%s), mouse-1: Display major mode menu"
                              (string-trim (format-mode-line minor-mode-alist)))))
  "Mode line construct for displaying active major mode.")

(put '+mode-line-major-mode 'risky-local-variable t)

(defvar-local +mode-line-misc-info
    '(:eval
      (let ((content (format-mode-line mode-line-misc-info)))
        (when (and (> (length content) 0) (mode-line-window-selected-p))
          (concat "   " (propertize content 'face '+mode-line-gray)))))
  "Mode line construct displaying `mode-line-misc-info'.
Specific to the current window's mode line.")

(put '+mode-line-misc-info 'risky-local-variable t)

(defvar-local +mode-line-flymake
    `(:eval
      (when (and (bound-and-true-p flymake-mode)
                 (mode-line-window-selected-p)
                 (length> (flymake-diagnostics) 0))
        '(:eval (flymake--mode-line-counters))))
  "Mode line construct for displaying flymake counters.
Specific to the current window's mode line.")

(put '+mode-line-flymake 'risky-local-variable t)

(with-eval-after-load 'eglot
  (setf (alist-get 'eglot--managed-mode mode-line-misc-info)
        '((" "
           (:eval
            (when (and (featurep 'eglot) (mode-line-window-selected-p))
              '(eglot--managed-mode eglot--mode-line-format)))
           " "))))

(with-eval-after-load 'vc
  (defun +strip-vc-backend-a (&rest _)
    "Strip backend name from `vc-mode' string."
    (when (stringp vc-mode)
      (setq vc-mode (concat
                     "  "
                     (replace-regexp-in-string
                      (format "^ %s." (vc-backend buffer-file-name))
                      "  " vc-mode)))))

  (advice-add #'vc-mode-line :after #'+strip-vc-backend-a))

(setq-default mode-line-format
              '("%e"
                +mode-line-kmacro
                +mode-line-narrow
                "  "
                mode-line-client
                mode-line-modified
                mode-line-remote
                mode-line-frame-identification
                mode-line-buffer-identification
                +mode-line-position
                mode-line-format-right-align
                "  "
                +mode-line-major-mode
                (vc-mode vc-mode)
                +mode-line-misc-info
                +mode-line-flymake
                "  "))

(define-minor-mode +hide-mode-line-mode
  "Toggle mode-line visibility in the current buffer."
  :init-value nil
  :global nil
  (if +hide-mode-line-mode
      (setq-local mode-line-format nil)
    (kill-local-variable 'mode-line-format)
    (force-mode-line-update)))

;;;; Themes

;;;;; Options

(setopt custom-safe-themes t) ; all themes are safe, right?

;;;;; Modus themes

(use-package modus-themes
  :ensure nil
  :bind (:map toggle-map ("t" . modus-themes-toggle))
  :custom
  (modus-themes-mixed-fonts t)
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs t)
  (modus-themes-headings '((0 1.20) (1 1.15) (2 1.10) (3 1.05)))
  ;; (modus-vivendi-palette-overrides '((bg-main "#101212"))) ; Sway
  (modus-vivendi-palette-overrides '((bg-main "#1E1E1E") (bg-dim "#242424"))) ; Gnome
  ;; (modus-vivendi-palette-overrides '((bg-main "#1A1E1F")
  ;;                                    (bg-tab-bar "#292D32")
  ;;                                    (bg-mode-line-active "#41454A")
  ;;                                    (bg-mode-line-inactive "#2D3134"))) ; Plasma
  (modus-themes-common-palette-overrides
   '((bg-tab-current bg-main)
     (bg-tab-other bg-tab-bar)
     (fringe unspecified)
     (border-mode-line-active unspecified)
     (border-mode-line-inactive unspecified))))

;; Load different theme depending on the active system theme
(if (string-match-p
     "prefer-dark"
     (shell-command-to-string "gsettings get org.gnome.desktop.interface color-scheme"))
    (load-theme 'modus-vivendi)
  (load-theme 'modus-operandi))

;;;; Fonts

(defun +setup-font-faces-h ()
  "Setup Emacs font faces."
  (when (display-graphic-p)
    (set-face-attribute 'default nil :font (font-spec :family "JetBrainsMono" :size 11.0 :weight 'regular))
    (set-face-attribute 'fixed-pitch nil :font (font-spec :family "JetBrainsMono" :size 11.0  :weight 'regular))
    (set-face-attribute 'variable-pitch nil :font (font-spec :family "AdwaitaSans" :size 12.0  :weight 'regular))))

(add-hook 'after-init-hook #'+setup-font-faces-h)
(add-hook 'server-after-make-frame-hook #'+setup-font-faces-h)

;;; General editing

;;;; Options

(setopt sentence-end-double-space nil   ; maybe a decade earlier
        select-enable-clipboard t)      ; merge system's and Emacs' clipboard

;;;; Encoding and text display

(setq-default default-buffer-file-coding-system 'utf-8
              buffer-file-coding-system 'utf-8
              bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right
              bidi-inhibit-bpa t)

;;;; Subword navigation

(use-package subword
  :ensure nil
  :defer 1
  :hook (after-init . global-subword-mode))

;;;; Delete selection

(use-package delsel
  :ensure nil
  :defer 1
  :hook (after-init . delete-selection-mode))

;;;; Tabs

(setq-default tab-width 2
              tab-always-indent 'complete ; enable completion using the TAB key.
              indent-tabs-mode nil)

;;;; Formatting

(setq-default fill-column 80          ; default that is still sane, even with 4k display
              word-wrap t             ; continue wrapped words at whitespace
              truncate-lines t)       ; do not display continuation lines

;;;; Parentheses

(use-package paren
  :ensure nil
  :defer 1
  :hook (after-init . show-paren-mode)
  :custom
  (show-paren-delay 0)
  (show-paren-context-when-offscreen t)
  (show-paren-when-point-inside-paren t))

;;;; Electric behavior

(use-package elec-pair
  :ensure nil
  :defer 1
  :hook
  (after-init    . electric-pair-mode)
  (org-mode      . +org-electric-pairs-h)
  (markdown-mode . +markdown-electric-pairs-h)
  :config
  (defun +add-electric-pairs-fn (pairs)
    "Add PAIRS to `electric-pair-pairs'."
    (setq-local electric-pair-pairs (append electric-pair-pairs pairs))
    (setq-local electric-pair-text-pairs electric-pair-pairs))

  (defun +markdown-electric-pairs-h ()
    (+add-electric-pairs-fn '((?* . ?*) (?_ . ?_) (?` . ?`))))

  (defun +org-electric-pairs-h ()
    (+add-electric-pairs-fn '((?/ . ?/) (?= . ?=) (?~ . ?~) (?_ . ?_)))))

(use-package puni
  :bind
  ("M-W"   . +puni-rewrap)
  ("M-K"   . +puni-kill)
  ("M-B"   . +puni-backward-kill)
  ("M-R"   . puni-raise)
  ("M-S"   . puni-split)
  ("M-U"   . puni-splice)
  ("M-I"   . puni-squeeze)
  ("M-M"   . puni-expand-region)
  ("M-N"   . puni-contract-region)
  ("C-("   . puni-slurp-backward)
  ("C-)"   . puni-slurp-forward)
  ("C-{"   . puni-barf-backward)
  ("C-}"   . puni-barf-forward)
  ("C-M-t" . puni-transpose)
  ("C-M-?" . puni-convolute)
  ("C-M-f" . puni-forward-sexp)
  ("C-M-b" . puni-backward-sexp)
  ("C-M-a" . puni-beginning-of-sexp)
  ("C-M-e" . puni-end-of-sexp)
  :custom (puni-blink-for-sexp-manipulating nil)
  :config
  (defun +puni-backward-kill ()
    "Kill until the beginning of a sexp."
    (interactive)
    (puni-delete-region
     (point)
     (car (puni-bounds-of-list-around-point))
     'kill))

  (defun +puni-kill ()
    "Kill until end of a sexp."
    (interactive)
    (puni-delete-region
     (point)
     (cdr (puni-bounds-of-list-around-point))
     'kill))

  (defun +puni-rewrap ()
    "Rewrap a sexp."
    (interactive)
    (pcase-let* ((c (read-char "Rewrap expression with: "))
                 (orig-point (point))
                 (`(,fst . ,lst) (pcase c
                                   (?\( '("("  . ")"))
                                   (?\{ '("{"  . "}"))
                                   (?\[ '("["  . "]"))
                                   (?\< '("<"  . ">"))
                                   (?\' '("'"  . "'"))
                                   (?\" '("\"" . "\""))
                                   (_ (error "Invalid character: %c" c)))))
      (puni-squeeze)
      (insert fst)
      (yank)
      (insert lst)
      (goto-char orig-point))))

(use-package change-inner
  :vc (:url "https://github.com/slotThe/change-inner")
  :bind ("M-i" . change-inner-around))

;;;; Simple

(use-package simple
  :ensure nil
  :bind
  ("M-u"     . upcase-dwim)
  ("M-l"     . downcase-dwim)
  ("M-c"     . capitalize-dwim)
  ("M-z"     . zap-up-to-char)
  ("M-Z"     . zap-to-char)
  ("M-L"     . duplicate-dwim)
  ("M-o"     . delete-blank-lines)
  ("C-x \\"  . align-regexp)
  ([remap keyboard-quit]  . +keyboard-quit)
  ([remap kill-ring-save] . +save-region-or-line)
  :custom
  (kill-region-dwim 'emacs-word)        ; Emacs 31
  (set-mark-command-repeat-pop t)
  (remote-file-name-inhibit-auto-save t)
  (remote-file-name-inhibit-auto-save-visited t)
  (save-interprogram-paste-before-kill t)
  :config
  (defun +save-region-or-line (beg end)
    "Save the region if active, otherwise save current line."
    (interactive "r")
    (if (region-active-p)
        (kill-ring-save beg end)
      (copy-region-as-kill
       (line-beginning-position)
       (line-end-position))))

  (defun +keyboard-quit (&optional interactive)
    "A sensible `keyboard-quit'."
    (interactive (list 'interactive))
    (let ((inhibit-quit t))
      (cond ((minibuffer-window-active-p (minibuffer-window))
             ;; quit the minibuffer if open.
             (when interactive
               (setq this-command 'abort-recursive-edit))
             (abort-recursive-edit))
            ;; don't abort macros
            ((or defining-kbd-macro executing-kbd-macro) nil)
            ;; Back to the default
            ((unwind-protect (keyboard-quit)
               (when interactive
                 (setq this-command 'keyboard-quit))))))))

;;;; Rainbow-mode

(use-package rainbow-mode
  :bind (:map toggle-map ("r" . rainbow-mode)))

;;; Navigation, search and extras

;;;; Options

(setopt xref-search-program 'ripgrep
        grep-use-headings t
        grep-program "rg")

;;;; Isearch

(use-package isearch
  :ensure nil
  :bind (:map isearch-mode-map ("M-/" . isearch-complete))
  :custom
  (isearch-lazy-count t)
  (search-whitespace-regexp ".*?")
  (isearch-allow-scroll 'unlimited)
  (isearch-repeat-on-direction-change t))

;;;; Outline

(use-package outline
  :ensure nil
  :hook
  (lisp-data-mode . +lisp-outline-h)
  (scheme-mode    . +lisp-outline-h)
  (lisp-data-mode . outline-minor-mode)
  (scheme-mode    . outline-minor-mode)
  :bind (:map toggle-map ("o" . outline-minor-mode))
  :custom
  (outline-minor-mode-highlight 'override)
  (outline-minor-mode-cycle t)
  :config
  (defun +lisp-outline-h ()
    (setq-local outline-regexp "[ \t]*;;;\\(;*\\**\\) [^ \t\n]")))

;;;; Occur

(use-package occur
  :ensure nil
  :hook (occur-mode . hl-line-mode))

;;;; Wgrep

(unless (>= emacs-major-version 31)
  (use-package wgrep
    :bind
    (:map grep-mode-map
          ("e"       . wgrep-change-to-wgrep-mode)
          ("C-x C-q" . wgrep-change-to-wgrep-mode))
    (:map wgrep-mode-map ("C-c C-c" . wgrep-finish-edit))
    :custom (wgrep-auto-save-buffer t)))

;;;; Avy

;; Just a thought... and you are there!
(use-package avy
  :bind
  ("M-j" . avy-goto-char-timer)
  (:map isearch-mode-map ("M-j" . avy-isearch))
  :custom (avy-background t))

;;; Frame, buffer and window management

;;;; Options

(setopt redisplay-skip-fontification-on-input t          ; inhibits fontification while receiving input
        focus-follows-mouse t                            ; pass information how focus is managed by WM
        next-screen-context-lines 0                      ; don't recenter point when scrolling
        scroll-preserve-screen-position t                ; keep screen position
        scroll-conservatively 10                         ; affects `scroll-step'
        scroll-margin 2)                                 ; it's nice to maintain a little margin

;;;; Scrolling

;; Enable smooth sailing
(use-package pixel-scroll
  :ensure nil
  :hook (after-init . pixel-scroll-precision-mode)
  :custom
  (pixel-scroll-precision-interpolate-page t)
  (pixel-scroll-precision-large-scroll-height 40.0))

;;;; Frame

(defun +toggle-frame-decorations ()
  "Toggle frame decorations."
  (interactive)
  (let ((decorations (frame-parameter nil 'undecorated)))
    (set-frame-parameter nil 'undecorated (not decorations))
    (message (if decorations
                 "Frame decorations enabled"
               "Frame decorations disabled"))))

(keymap-set toggle-map "D" #'+toggle-frame-decorations)

;;;; Buffer

;;;;; Uniquify

(use-package uniquify
  :ensure nil
  :defer 1
  :custom
  (uniquify-buffer-name-style 'forward)
  (uniquify-trailing-separator-p t))

;;;;; Ibuffer

(use-package ibuffer
  :ensure nil
  :hook (ibuffer-mode . hl-line-mode)
  :bind
  ("C-x C-b" . ibuffer)
  :custom
  (ibuffer-expert t)
  (ibuffer-display-summary nil)
  (ibuffer-use-other-window nil)
  (ibuffer-use-header-line t)
  (ibuffer-show-empty-filter-groups nil)
  (ibuffer-movement-cycle nil)
  (ibuffer-default-sorting-mode 'mode)
  (ibuffer-saved-filter-groups nil)
  (ibuffer-old-time 48))

;;;;; Revert all buffers

(use-package autorevert
  :ensure nil
  :defer 1
  :hook (after-init . global-auto-revert-mode)
  :custom (global-auto-revert-non-file-buffers t))

;;;;; Temporary buffers

(defun +create-temporary-buffer ()
  "Create a temporary buffer with a specified major mode."
  (interactive)
  (let* ((modes (seq-remove
                 (lambda (element)
                   (or (not (symbolp element))
                       (eq element t)
                       (eq element nil)))
                 (mapcar #'cdr auto-mode-alist)))
         (mode-name (completing-read "Major mode: " (delete-dups modes)))
         (mode-symbol (intern mode-name))
         (buffer-name (generate-new-buffer-name (format "*temp-%s*" mode-name))))
    (with-current-buffer (get-buffer-create buffer-name)
      (funcall mode-symbol)
      (switch-to-buffer (current-buffer)))))

(keymap-set open-map "*" '+create-temporary-buffer)

;;;; Windows

;;;;; Window

(use-package window
  :ensure nil
  :bind
  ("C-x O"   . +other-other-window)
  ("C-x k"   . kill-current-buffer)
  ("C-x K"   . kill-buffer)
  ("C-c q r" . restart-emacs)
  ("C-c q q" . save-buffers-kill-emacs)
  ("C-x 4 s" . +toggle-window-split)
  ("C-x 4 t" . +transpose-windows)
  (:repeat-map other-window-repeat-map
               ("o" . other-window)
               ("O" . +other-other-window))
  :custom
  (window-resize-pixelwise t)
  (window-combination-resize t)  ; sane splitting for windows
  (recenter-positions '(top middle bottom))
  (switch-to-buffer-in-dedicated-window 'pop)
  :config
  (defun +other-other-window ()
    "Go to previous window."
    (interactive)
    (other-window -1))

  (defun +toggle-window-split ()
    "Toggle window split from vertical to horizontal, or vice versa."
    (interactive)
    (if (= (count-windows) 2)
        (let* ((this-win-buffer (window-buffer))
               (next-win-buffer (window-buffer (next-window)))
               (this-win-edges (window-edges (selected-window)))
               (next-win-edges (window-edges (next-window)))
               (this-win-2nd (not (and (<= (car this-win-edges)
                                           (car next-win-edges))
                                       (<= (cadr this-win-edges)
                                           (cadr next-win-edges)))))
               (splitter
                (if (= (car this-win-edges)
                       (car (window-edges (next-window))))
                    'split-window-horizontally
                  'split-window-vertically)))
          (delete-other-windows)
          (let ((first-win (selected-window)))
            (funcall splitter)
            (if this-win-2nd (other-window 1))
            (set-window-buffer (selected-window) this-win-buffer)
            (set-window-buffer (next-window) next-win-buffer)
            (select-window first-win)
            (if this-win-2nd (other-window 1))))))

  (defun +transpose-windows ()
    "Swap the buffers shown in current and next window."
    (interactive)
    (let ((this-buffer (window-buffer))
          (next-window (next-window nil :no-minibuf nil)))
      (set-window-buffer nil (window-buffer next-window))
      (set-window-buffer next-window this-buffer)
      (select-window next-window))))

(use-package windmove
  :ensure nil
  :defer 1
  :init (windmove-default-keybindings '(nil . (shift)))
  :custom
  (windmove-swap-states-default-keybindings '(nil . (control shift)))
  (windmove-delete-default-keybindings '(nil . (meta control shift))))

;;;;; Tab-bar

(use-package tab-bar
  :ensure nil
  :bind
  ("C-c <left>"  . tab-bar-history-back)
  ("C-c <right>" . tab-bar-history-forward)
  (:repeat-map tab-bar-repeat-map
               ("<left>"  . tab-bar-history-back)
               ("<right>" . tab-bar-history-forward))
  :custom
  (tab-bar-show 1)
  (tab-bar-separator " ")
  (tab-bar-history-mode t)
  (tab-bar-auto-width nil)
  (tab-bar-new-button-show nil)
  (tab-bar-close-button-show nil)
  (tab-bar-tab-name-truncated-max 25)
  (tab-bar-format '(tab-bar-format-tabs tab-bar-separator))
  (tab-bar-tab-name-format-function '+tab-bar-tab-name-padded-fn)
  :config
  (defun +tab-bar-tab-name-padded-fn (tab i)
    (propertize (concat " " (tab-bar-tab-name-format-default tab i) " ")
                'face (funcall tab-bar-tab-face-function tab))))

;;;;; Popups

(use-package popper
  :defer 1
  :hook (after-init . popper-mode)
  :bind
  ("C-\\"    . popper-toggle)
  ("M-\\"    . popper-cycle)
  ("C-M-\\"  . popper-toggle-type)
  ("C-c C-z" . +switch-to-popup)
  :custom
  (popper-echo-mode t)
  (popper-mode-line nil)
  (popper-window-height 0.33)
  (popper-reference-buffers
   '("\\*Agenda Commands\\*" "\\*Org Select\\*" "\\*Capture\\*" "^CAPTURE-.*\\.org*"
     "\\*Shell Command Output\\*" "Output\\*$" "\\*vc-.*\\**" "\\*eldoc\\*"
     "^\\*eshell.*\\*$" "^\\*.*shell.*\\*.*$" "^\\*.*vterm.*\\*" "^\\*sly-[^s]"
     ("\\*Async Shell Command\\*" . hide)
     compilation-mode comint-mode finder-mode help-mode
     flymake-diagnostics-buffer-mode occur-mode grep-mode
     geiser-repl-mode cider-repl-mode inf-ruby-mode))
  :config
  (defun +switch-to-popup ()
    "Switch between the last focused window and the popup window."
    (interactive)
    (let ((mru-window (get-mru-window nil nil 'not-this-one-dummy))
          (current-popup (caar popper-open-popup-alist)))
      (if current-popup
          (if (eq (selected-window) current-popup)
              (select-window mru-window)
            (select-window current-popup))))))

;;; Completion

;;;; Minibuffer

(use-package minibuffer
  :ensure nil
  :hook (minibuffer-setup . cursor-intangible-mode)
  :custom
  (completion-ignore-case t)           ; obviously, ignore case
  (completion-auto-select 'second-tab) ; select the *Completions* buffer after second <TAB>
  (completion-auto-help 'visible)      ; always show *Completions* after completion attempt
  (completion-show-help nil)           ; no help message in *Completions*
  (completions-detailed t)             ; display completions with details
  (completions-max-height 20)          ; otherwise they will take everything
  (completions-header-format nil)      ; no header, please
  (completions-format 'one-column)     ; vertico style!
  (completions-sort 'historical)       ; sort candidates by most recent

  ;; Add `substring' to default `completion-styles'
  (completion-styles '(basic substring partial-completion emacs22))

  ;; Tweak minibuffer behavior
  (resize-mini-windows t)                    ; allow resizing of mini-windows
  (enable-recursive-minibuffers t)           ; enable recursive minibuffers
  (read-buffer-completion-ignore-case t)     ; ignore case when reading buffer name
  (read-file-name-completion-ignore-case t)  ; ignore case whn reading file name
  (minibuffer-depth-indicate-mode t)         ; show recursion depth in minibuffer prompt
  (minibuffer-electric-default-mode t)       ; show default value when it's applicable
  (minibuffer-eldef-shorten-default t)       ; shorten "(default ...)" to "[...]" in minibuffer prompts
  (minibuffer-visible-completions t)         ; use arrows to navigate *Completions* buffer

  ;; Do not allow the cursor in the minibuffer prompt
  (minibuffer-prompt-properties
   '(read-only t cursor-intangible t face minibuffer-prompt))

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  (read-extended-command-predicate
   #'command-completion-default-include-p))

;;;; Vertico

(use-package vertico
  :hook
  (after-init                 . vertico-mode)
  (minibuffer-setup           . vertico-repeat-save)
  (rfn-eshadow-update-overlay . vertico-directory-tidy)
  :bind
  ("C-c r" . vertico-repeat)
  (:map vertico-map
        ("M-j"   . vertico-quick-exit)
        ("C-M-n" . vertico-next-group)
        ("C-M-p" . vertico-previous-group)
        ("DEL"   . vertico-directory-delete-char))
  :custom
  (vertico-resize nil)
  (vertico-cycle t)
  (vertico-count 15))

;;;; Orderless

(use-package orderless
  :after vertico
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles basic partial-completion)))))

;;;; Marginalia

(use-package marginalia
  :defer 1
  :hook (after-init . marginalia-mode))

;;;; Consult

(use-package consult
  :hook (completion-list-mode . consult-preview-at-point-mode)
  :bind
  ("M-y"   . consult-yank-pop)
  ("M-'"   . consult-register-store)
  ("M-#"   . consult-register-load)
  ("C-M-#" . consult-register)
  ("M-?"   . +consult-ripgrep-dwim)
  ([remap Info-search] . consult-info)
  (:map toggle-map ("T" . consult-theme))
  (:map ctl-x-map
        ("M-:" . consult-complex-command)
        ("C-r" . consult-recent-file)
        ("b"   . consult-buffer)
        ("r b" . consult-bookmark)
        ("4 b" . consult-buffer-other-window)
        ("5 b" . consult-buffer-other-frame)
        ("t b" . consult-buffer-other-tab))
  (:map goto-map
        ("e"   . consult-compile-error)
        ("f"   . consult-flymake)
        ("g"   . consult-goto-line)
        ("M-g" . consult-goto-line)
        ("o"   . consult-outline)
        ("i"   . consult-imenu)
        ("I"   . consult-imenu-multi)
        ("m"   . consult-mark)
        ("M"   . consult-global-mark))
  (:map search-map
        ("f" . consult-fd)
        ("g" . consult-grep)
        ("G" . consult-git-grep)
        ("r" . consult-ripgrep)
        ("l" . consult-line)
        ("L" . consult-line-multi)
        ("k" . consult-keep-lines)
        ("u" . consult-focus-lines)
        ("e" . consult-isearch-history))
  (:map isearch-mode-map
        ("M-s l" . consult-line)
        ("M-s L" . consult-line-multi)
        ("M-s e" . consult-isearch-history))
  (:map project-prefix-map
        ("g" . consult-ripgrep)
        ("b" . consult-project-buffer))
  (:map minibuffer-local-map
        ("M-s" . consult-history)
        ("M-r" . consult-history))
  :custom
  (register-preview-delay 0.5)
  (register-preview-function #'consult-register-format)
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref)
  (completion-in-region-function #'consult-completion-in-region)
  (consult-narrow-key "<")
  :config
  (defun +consult-ripgrep-dwim (&optional dir initial)
    "Search for an active region or a symbol at point if no region is active."
    (interactive (list current-prefix-arg
                       (if (use-region-p)
                           (progn
                             (deactivate-mark)
                             (buffer-substring-no-properties
                              (region-beginning)
                              (region-end)))
                         (when-let* ((s (symbol-at-point)))
                           (symbol-name s)))))
    (xref-push-marker-stack)
    (consult-ripgrep dir initial))

  ;; Tweak the register preview window.
  (advice-add #'register-preview :override #'consult-register-window))

(use-package consult-dir
  :bind
  ("C-x C-d" . consult-dir)
  (:map vertico-map
        ("C-x C-d" . consult-dir)
        ("C-x C-j" . consult-dir-jump-file))
  :custom (consult-dir-shadow-filenames nil))

;;;; Embark

(use-package embark
  :after vertico
  :defer 1
  :bind
  ("C-."   . embark-act)
  ("C-c ." . embark-act)
  ("C-,"   . embark-dwim)
  ("C-c ," . embark-dwim)
  ("C-:"   . embark-act-all)
  ("C-c :" . embark-act-all)
  ("C-h B" . embark-bindings)
  (:map minibuffer-local-map
        ("C-SPC"   . +embark-preview)
        ("C->"     . embark-become)
        ("C-c >"   . embark-become)
        ("C-c C-l" . embark-collect)
        ("C-c C-o" . embark-export))
  (:map embark-file-map ("U" . +0x0-upload-file))
  (:map embark-region-map ("U" . +0x0-upload-text))
  (:map embark-general-map ("h" . describe-symbol))
  :custom
  (prefix-help-command #'embark-prefix-help-command)
  (embark-cycle-key ".")
  (embark-confirm-act-all nil)
  (embark-indicators '(embark-minimal-indicator
                       embark-highlight-indicator
                       embark-isearch-highlight-indicator))
  :config
  (defvar embark-quit-after-action)

  (defun +embark-preview ()
    "Previews candidate, unless it's a consult command"
    (interactive)
    (unless (bound-and-true-p consult--preview-function)
      (save-selected-window
        (let (embark-quit-after-action)
          (embark-dwim)))))

  (defun +copy-grep-results-as-kill-fn (strings)
    (embark-copy-as-kill
     (mapcar (lambda (string)
               (substring string
                          (1+ (next-single-property-change
                               (1+ (next-single-property-change 0 'face string))
                               'face string))))
             strings)))

  (add-to-list 'embark-multitarget-actions '+copy-grep-results-as-kill-fn)

  (defvar-keymap embark-consult-grep-map
    :doc "Keymap for actions for consult-grep results."
    :parent embark-general-map
    "w" #'+copy-grep-results-as-kill-fn)

  (setf (alist-get 'consult-grep embark-keymap-alist) 'embark-consult-grep-map)

  (defun store-input-for-consult ()
    (when (memq minibuffer-history-variable
                '(consult--grep-history
                  consult--line-history
                  consult--find-history
                  consult--man-history))
      (add-to-history minibuffer-history-variable (minibuffer-contents))))

  (advice-add #'embark-export  :before #'store-input-for-consult)
  (advice-add #'embark-collect :before #'store-input-for-consult)

  (defun +0x0-upload-text ()
    (interactive)
    (let* ((contents
            (if (use-region-p)
                (buffer-substring-no-properties (region-beginning) (region-end))
              (buffer-string)))
           (temp-file (make-temp-file "0x0" nil ".txt" contents)))
      (message "Sending %s to 0x0.st..." temp-file)
      (let ((url (string-trim-right
                  (shell-command-to-string
                   (format "curl -s -F'file=@%s' -Fsecret= -Fexpires=24 https://0x0.st"
                           temp-file)))))
        (message "Yanked ‘%s’ into kill ring." url)
        (kill-new url)
        (delete-file temp-file))))

  (defun +0x0-upload-file (file-path)
    (interactive "fSelect a file to upload: ")
    (message "Sending %s to 0x0.st..." file-path)
    (let ((url (string-trim-right
                (shell-command-to-string
                 (format "curl -s -F'file=@%s' -Fsecret= -Fexpires=24 https://0x0.st"
                         (expand-file-name file-path))))))
      (message "Yanked ‘%s’ into kill ring." url)
      (kill-new url))))

(use-package embark-consult
  :hook (embark-collect-mode . consult-preview-at-point-mode))

;;;; Corfu

(use-package corfu
  :after savehist
  :init (global-corfu-mode)
  :bind (:map corfu-map ("M-j" . corfu-quick-insert))
  :custom
  (corfu-cycle t)
  (corfu-max-width 120)
  (corfu-auto-delay 0.5)
  (corfu-history-mode t)
  (corfu-popupinfo-mode t)
  (corfu-popupinfo-delay '(1.25 . 0.5))
  :config (add-to-list 'savehist-additional-variables 'corfu-history))

;;;; Cape

(use-package cape
  :init
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  :hook
  (text-mode          . +text-capf-h)
  (eglot-managed-mode . +eglot-capf-h)
  :custom
  (cape-dabbrev-min-length 3)
  (cape-dict-file (expand-file-name "dict/words" user-emacs-directory))
  :config
  (defun +eglot-capf-h ()
    (setq-local completion-at-point-functions
                (list (cape-capf-super
                       #'tempel-expand
                       #'eglot-completion-at-point
                       #'cape-dabbrev
                       #'cape-file))))

  (defun +text-capf-h ()
    (setq-local completion-at-point-functions
                (list (cape-capf-super
                       #'tempel-expand
                       #'cape-dabbrev
                       #'cape-file
                       #'cape-dict)))))

;;;; Pcmpl-args

(use-package pcmpl-args
  :after vertico)

;;;; Tempel

(use-package tempel
  :hook ((conf-mode prog-mode text-mode) . +tempel-setup-capf-h)
  :bind ("M-+" . tempel-expand)
  :init
  (defun +tempel-setup-capf-h ()
    (setq-local completion-at-point-functions
                (cons #'tempel-expand
                      completion-at-point-functions))))

;;;; Dabbrev

(use-package dabbrev
  :ensure nil
  :custom
  (dabbrev-backward-only nil)
  (dabbrev-case-fold-search nil)
  (dabbrev-upcase-means-case-search t))

;;; Writing

;;;; Focus mode

;;;;; Olivetti

(use-package olivetti
  :bind
  (:map toggle-map
        ("e" . olivetti-mode)           ; "écrits" mnemonic (as in writing mode)
        ("E" . +olivetti-focus-mode))
  :custom
  (olivetti-body-width 100)
  (olivetti-minimum-body-width 80)
  (olivetti-recall-visual-line-mode-entry-state t)
  :preface
  (define-minor-mode +olivetti-focus-mode
    "Toggle buffer-local `olivetti-mode' with additional parameters."
    :init-value nil
    :global nil
    (if +olivetti-focus-mode
        (progn
          (olivetti-mode 1)
          (diff-hl-mode 0)
          (unless (derived-mode-p 'prog-mode)
            (+bar-cursor-mode 1)
            (+hide-mode-line-mode 1)))
      (olivetti-mode 0)
      (diff-hl-mode 1)
      (unless (derived-mode-p 'prog-mode)
        (+bar-cursor-mode 0)
        (+hide-mode-line-mode 0)))))

;;;; Spellcheck

(setopt text-mode-ispell-word-completion nil)

;; Tases you if you forget a letter.
(use-package jinx
  :hook (text-mode git-commit-mode)
  :bind
  ([remap ispell-word] . jinx-correct)
  (:map toggle-map ("s" . jinx-mode))
  :custom (jinx-languages "en_US pl_PL"))

;;;; Dictionary

(use-package dictionary
  :ensure nil
  :bind (:map open-map ("d" . dictionary-search))
  :custom
  (dictionary-server "dict.org")
  (dictionary-create-buttons nil)
  (dictionary-use-single-buffer t)
  (dictionary-default-popup-strategy "lev"))

;;;; Org-mode

;;;;; Core

(use-package org
  :hook (org-mode . turn-on-visual-line-mode)
  :bind
  (:map org-mode-map
        ("C-c ." . nil)
        ("C-c ," . nil))
  :custom
  (org-directory "~/Documents/Org/")
  (org-fold-catch-invisible-edits 'show-and-error)
  (org-insert-heading-respect-content t)
  (org-fontify-quote-and-verse-blocks t)
  (org-ellipsis "…")
  (org-capture-bookmark nil)
  (org-mouse-1-follows-link t)
  (org-use-speed-commands t)
  (org-special-ctrl-o t)
  (org-src-fontify-natively t)
  (org-src-tab-acts-natively nil)
  (org-src-preserve-indentation nil)
  (org-src-window-setup 'other-window)
  (org-edit-src-persistent-message nil)
  (org-refile-targets '(("archive.org" :maxlevel . 1)))
  (org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)" "CANCELLED(c)")))
  (org-structure-template-alist
   '(("s" . "src")
     ("sh" . "src sh")
     ("el" . "src emacs-lisp")
     ("haskell" . "src haskell")
     ("yaml" . "src yaml")
     ("json" . "src json")
     ("x" . "example")
     ("q" . "quote")))
  (org-capture-templates
   '(("t" "Task" entry (file "~/Documents/Org/tasks.org")
      "* TODO %? \n%i")
     ("d" "Deadline" entry (file "~/Documents/Org/tasks.org")
      "* TODO %? \n  DEADLINE: %^t")))
  :config (advice-add #'org-refile :after 'org-save-all-org-buffers))

;;;;; Agenda

(use-package org-agenda
  :ensure nil
  :bind
  ("C-c x" . org-capture)
  (:map open-map ("a" . org-agenda))
  :custom
  (org-agenda-files (list org-directory))
  (org-agenda-compact-blocks nil)
  (org-agenda-window-setup 'current-window)
  (org-agenda-skip-unavailable-files t)
  (org-agenda-span 10)
  (org-agenda-tags-column 0)
  (calendar-week-start-day 1)
  (org-agenda-start-on-weekday nil)
  (org-agenda-start-day "-3d")
  (org-agenda-inhibit-startup t))

;;;;; Org-cite

(use-package oc
  :ensure nil
  :commands (org-cite-insert)
  :custom (org-cite-global-bibliography
           '("~/Documents/Org/bibliography.bib")))

;;;;; Denote

(use-package denote
  :after embark
  :hook (dired-mode . denote-dired-mode-in-directories)
  :bind
  ("C-c n n" . denote)
  ("C-c n d" . denote-date)
  ("C-c n c" . denote-region)
  ("C-c n s" . +denote-search)
  ("C-c n o" . denote-sort-dired)
  ("C-c n r" . denote-rename-file)
  ("C-c n R" . denote-rename-file-using-front-matter)
  ("C-c n j" . denote-journal-extras-new-entry)
  ("C-c n J" . denote-journal-extras-new-or-existing-entry)
  (:map embark-region-map ("D" . denote-region))
  (:map open-map ("n" . denote-open-or-create))
  (:map text-mode-map
        ("C-c n i"   . denote-link)
        ("C-c n I"   . denote-add-links)
        ("C-c n b"   . denote-backlinks)
        ("C-c n f f" . denote-find-link)
        ("C-c n f b" . denote-find-backlink))
  :custom
  (denote-save-buffers t)
  (denote-menu-bar-mode nil)
  (denote-known-keywords '())
  (denote-rename-confirmations nil)
  (denote-prompts '(title subdirectory))
  (denote-history-completion-in-prompts nil)
  (denote-dired-directories-include-subdirectories t)
  (denote-directory "~/Documents/Org/files")
  (denote-dired-directories (list denote-directory))
  (denote-journal-extras-directory "~/Documents/Org/files/journals")
  (denote-journal-extras-title-format nil)
  (denote-journal-extras-keyword nil)
  :config
  (defun +denote-search ()
    "Seach content of notes in `denote-directory'"
    (interactive)
    (consult-ripgrep denote-directory)))

;;;;; Markdown

(use-package markdown-mode
  :mode
  ("README\\.md\\'" . gfm-mode)
  ("\\.md\\'"       . markdown-mode)
  ("\\.markdown\\'" . markdown-mode)
  :hook
  (markdown-mode . outline-minor-mode)
  (markdown-mode . turn-on-visual-line-mode)
  :custom (markdown-fontify-code-blocks-natively t))

;;; Utility

;; top for Emacs
(use-package proced
  :ensure nil
  :custom
  (proced-enable-color-flag t)
  (proced-auto-update-flag 'visible))

(use-package lingva
  :bind (:map open-map ("l" . lingva-translate))
  :custom (lingva-instance "https://lingva.lunar.icu"))

(use-package gptel
  :if (locate-library "private")
  :load private
  :commands (gptel)
  :bind (:map open-map ("g" . +gptel))
  :custom
  (gptel-default-mode 'org-mode)
  (gptel-org-branching-context t)
  (gptel-model "claude-3-7-sonnet-20250219")
  (gptel-prompt-prefix-alist '((org-mode . "*Prompt*:\n")))
  (gptel-response-prefix-alist '((org-mode . "*Response*:\n")))
  :config
  (setopt gptel--system-message +default-directive
          gptel-directives +directives
          gptel-backend
          (gptel-make-anthropic "Claude"
            :stream t
            :key (+auth-get-field-fn "claude.anthropic.com" :secret)))

  (defun +gptel (&optional arg)
    "Open gptel buffer. If ARG is non-nil, prompt for a new session name."
    (interactive "P")
    (if arg
        (call-interactively #'gptel)
      (pop-to-buffer-same-window (gptel "*Claude*")))))

(use-package ledger-mode)

;;; Browsing

(use-package browse-url
  :ensure nil
  :custom
  (browse-url-browser-function 'browse-url-firefox)
  (browse-url-firefox-program "flatpak-spawn")
  (browse-url-firefox-arguments '("--host" "flatpak" "run" "org.mozilla.firefox")))

(use-package shr
  :ensure nil
  :custom
  (shr-use-colors nil)
  (shr-max-image-proportion 1.0)
  (shr-image-animate nil)
  (shr-inhibit-images t)
  (shr-width nil)
  (shr-cookie-policy nil))

(use-package eww
  :ensure nil
  :bind (:map open-map ("b" . eww))
  :custom
  (eww-search-prefix "https://html.duckduckgo.com/html/?q=")
  (eww-download-directory (expand-file-name "~/Downloads"))
  (eww-browse-url-new-window-is-tab nil)
  (eww-history-limit 150)
  (url-privacy-level 'high))

(use-package elpher
  :bind (:map open-map ("G" . elpher)))

;;; RSS

(use-package elfeed
  :if (locate-library "private")
  :load private
  :bind (:map open-map ("f" . elfeed))
  :custom
  (elfeed-search-filter "@1-year-ago +unread ")
  (elfeed-db-directory (concat user-emacs-directory "elfeed/"))
  :config (setopt elfeed-feeds elfeed-feeds-alist))

;;; IRC

(use-package rcirc
  :ensure nil
  :hook (rcirc-mode . rcirc-omit-mode)
  :bind (:map open-map ("i" . irc))
  :custom
  (rcirc-fill-column 90)
  (rcirc-reconnect-delay 10)
  (rcirc-kill-channel-buffers t)
  (rcirc-default-nick "alternateved")
  (rcirc-default-full-name user-full-name)
  (rcirc-default-user-name user-login-name)
  (rcirc-track-ignore-server-buffer-flag t)
  (rcirc-omit-unless-requested '("TOPIC" "NAMES"))
  (rcirc-omit-responses '("JOIN" "PART" "QUIT" "NICK" "AWAY"))
  (rcirc-server-alist
   `(("irc.libera.chat"
      :port 6697 :encryption tls
      :user-name "alternateved"
      :password ,(+auth-get-field-fn "irc.libera.chat" :secret)))))

;;; Email

;;;; Message

(use-package message
  :ensure nil
  :hook
  (message-setup . turn-off-auto-fill)
  (message-setup . turn-on-visual-line-mode)
  (message-setup . mml-secure-message-sign)
  :custom
  (message-directory "~/.mail/alternateved")
  (message-kill-buffer-on-exit t)
  (message-sendmail-envelope-from 'header)
  (message-wide-reply-confirm-recipients t)
  (message-citation-line-function #'message-insert-formatted-citation-line)
  (message-citation-line-format "\nOn %a, %b %d %Y, %N wrote:\n")
  (mml-secure-openpgp-sign-with-sender t)
  (mml-secure-openpgp-encrypt-to-self t)
  (mml-secure-smime-encrypt-to-self t)
  (mml-secure-smime-sign-with-sender t))

;;;; Sendmail

(use-package sendmail
  :ensure nil
  :custom
  (mail-specify-envelope-from t)
  (smtpmail-smtp-server "smtp.migadu.com")
  (smtpmail-smtp-service 465)
  (smtpmail-stream-type 'ssl)
  (send-mail-function 'smtpmail-send-it))

;;;; Notmuch

(use-package notmuch
  :ensure nil
  :if (locate-library "notmuch")
  :hook (notmuch-message-mode . notmuch-mua-attachment-check)
  :bind (:map open-map ("m" . notmuch))
  :custom
  ;; General UI
  (notmuch-show-logo nil)
  (notmuch-column-control t)
  (notmuch-hello-auto-refresh t)
  (notmuch-show-all-tags-list t)
  (notmuch-hello-sections '
   (notmuch-hello-insert-header
    notmuch-hello-insert-saved-searches
    notmuch-hello-insert-alltags))

  ;; Results format
  (notmuch-search-result-format
   '(("date" . "%12s ")
     ("count" . "%-7s ")
     ("authors" . "%-20s ")
     ("subject" . "%-70s ")
     ("tags" . "(%s)")))
  (notmuch-tree-result-format
   '(("date" . "%12s ")
     ("authors" . "%-20s ")
     ((("tree" . "%s")
       ("subject" . "%s"))
      . " %-80s ")
     ("tags" . "(%s)")))

  ;; Search functionality
  (notmuch-search-oldest-first nil)
  (notmuch-saved-searches
   '((:name "inbox"    :query "tag:inbox not tag:trash" :key "i")
     (:name "flagged"  :query "tag:flagged"             :key "f")
     (:name "sent"     :query "tag:sent"                :key "s")
     (:name "drafts"   :query "tag:drafts"              :key "d")
     (:name "archived" :query "tag:archived"            :key "a")))

  ;; Tags functionality
  (notmuch-archive-tags '("+archived" "-inbox" "-unread" "-spam" "-deleted"))
  (notmuch-message-replied-tags '("+replied"))
  (notmuch-message-forwarded-tags '("+forwarded"))
  (notmuch-show-mark-read-tags '("-unread"))
  (notmuch-draft-tags '("+drafts"))
  (notmuch-tagging-keys
   '(("u" ("+unread") "Mark as unread")
     ("f" ("+flagged" "-unread") "Flag as important")
     ("r" notmuch-show-mark-read-tags "Mark as read")
     ("s" ("+spam" "-inbox" "-unread" "-archived" "-deleted") "Mark as spam")
     ("a" notmuch-archive-tags "Mark as archived")
     ("i" ("+inbox" "-deleted" "-archived" "-spam") "Mark as inbox")
     ("d" ("+deleted" "-inbox" "-archived" "-unread" "-spam") "Mark for deletion")))

  ;; Reading messages
  (notmuch-show-relative-dates t)
  (notmuch-message-headers-visible t)

  ;; Configure default mail-user-agent
  (mail-user-agent 'notmuch-user-agent)

  ;; Email composition
  (notmuch-always-prompt-for-sender t)
  (notmuch-mua-compose-in 'current-window)
  (notmuch-mua-user-agent-function 'notmuch-mua-user-agent-full)
  (notmuch-mua-cite-function 'message-cite-original-without-signature)
  (notmuch-mua-attachment-regexp
   "\\b\\(attachement\\|attached\\|attach\\|załącznik?u\\|załączam\\|załączonym\\)\\b")

  ;; Directory for drafts and sent messages
  (notmuch-draft-folder "Drafts")
  (notmuch-fcc-dirs "alternateved/Sent"))

;;; Shells and terms

;;;; Eshell

(use-package eshell
  :ensure nil
  :load em-hist
  :bind
  (:map open-map ("e" . eshell))
  (:map eshell-hist-mode-map
        ("M-r" . consult-history)
        ("M-s" . consult-history))
  :custom
  (eshell-history-size 10000)
  (eshell-banner-message "")
  (eshell-kill-processes-on-exit t)
  (eshell-scroll-to-bottom-on-input t)
  (eshell-highlight-prompt t)
  (eshell-hist-ignoredups t)
  (eshell-error-if-no-glob t)
  (eshell-destroy-buffer-when-process-dies t)
  :config (setenv "PAGER" "cat"))

;;;; Shell

(use-package shell
  :ensure nil
  :custom (shell-file-name "bash"))

;;;; VTerm

(use-package vterm
  :bind
  (:map vterm-mode-map
        ("C-\\"   . nil)
        ("M-\\"   . nil)
        ("C-M-\\" . nil))
  (:map open-map ("t" . vterm))
  (:map project-prefix-map ("t" . +vterm-project))
  :custom
  (vterm-always-compile-module t)
  (vterm-kill-buffer-on-exit t)
  (vterm-max-scrollback 10000)
  :preface
  (defun +vterm-project ()
    "Run `vterm' on project.
If a buffer already exists for running a vterm shell in the project's root,
switch to it. Otherwise, create a new vterm shell."
    (interactive)
    (let* ((default-directory (project-root (project-current)))
           (default-project-vterm-name (project-prefixed-buffer-name "vterm"))
           (vterm-buffer (get-buffer default-project-vterm-name)))
      (if (and vterm-buffer (not current-prefix-arg))
          (pop-to-buffer-same-window vterm-buffer)
        (vterm default-project-vterm-name)))))

;;;; Exec path from shell

;; Ensure environment variables inside Emacs are the same as in the user’s shell.
(use-package exec-path-from-shell
  :defer 1
  :hook (after-init . exec-path-from-shell-initialize)
  :custom
  (exec-path-from-shell-arguments '("-l"))
  (exec-path-from-shell-variables '("PATH" "SSH_AUTH_SOCK")))

;;;; Tramp

(use-package tramp
  :ensure nil
  :custom
  (tramp-default-remote-shell "/bin/bash")
  (tramp-terminal-type "tramp")
  (tramp-default-method "ssh")
  (tramp-verbose 6))

;;;; Add interaction with clipboard

(use-package xclip
  :unless (display-graphic-p)
  :hook (tty-setup)
  :custom (xclip-method 'wl-copy))

;;;; Add mouse support in terminal

(add-hook 'tty-setup-hook #'xterm-mouse-mode)

;;;; Make shebang (#!) file executable when saved

(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;;; Version control

;;;; Options

(setopt vc-follow-symlinks t)

;;;; Magit

;; Famous vim killer is coming to town
(use-package magit
  :bind
  ("C-x g" . magit-status)
  (:map project-prefix-map ("m" . magit-project-status))
  :custom
  (magit-diff-refine-hunk t)
  (magit-save-repository-buffers 'dontask)
  (magit-revision-insert-related-refs nil)
  (magit-section-visibility-indicator nil)
  (magit-section-initial-visibility-alist '((untracked . show) (stashes . hide)))
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;;;; Vundo

(use-package vundo
  :bind ("C-x u" . vundo)
  :custom (vundo-glyph-alist vundo-unicode-symbols))

;;;; Ediff

(use-package ediff
  :ensure nil
  :custom
  (ediff-merge-split-window-function 'split-window-horizontally)
  (ediff-split-window-function 'split-window-horizontally)
  (ediff-window-setup-function 'ediff-setup-windows-plain))

;;;; Diff-hl

;; Show me those gutters!
(use-package diff-hl
  :defer 1
  :hook
  (after-init         . global-diff-hl-mode)
  (after-init         . (lambda () (unless (window-system) (diff-hl-margin-mode))))
  (dired-mode         . diff-hl-dired-mode)
  (magit-pre-refresh  . diff-hl-magit-pre-refresh)
  (magit-post-refresh . diff-hl-magit-post-refresh)
  :custom
  (diff-hl-draw-borders nil)
  (diff-hl-margin-symbols-alist
   '((insert  . " ")
     (delete  . " ")
     (change  . " ")
     (unknown . " ")
     (ignored . " "))))

;;; Programming

;;;; Language Server Protocol

(use-package eglot
  :ensure nil
  :bind
  (:map code-map
        ("l" . eglot)
        ("h" . eldoc)
        ("R" . eglot-rename)
        ("q" . eglot-shutdown)
        ("a" . eglot-code-actions))
  :custom
  (jsonrpc-event-hook nil)
  (fset #'jsonrpc--log-event #'ignore)
  (eglot-autoshutdown t)
  (eglot-sync-connect 1)
  (eglot-extend-to-xref t)
  (eglot-events-buffer-config '(:size 0 :format full))
  (eglot-confirm-server-initiated-edits nil)
  (eglot-code-action-indications '(eldoc-hint))
  (eglot-ignored-server-capabilities
   '(:colorProvider
     :codeLensProvider
     :foldingRangeProvider
     :documentHighlightProvider
     :documentFormattingProvider
     :documentRangeFormattingProvider
     :documentOnTypeFormattingProvider)))

;;;; Tree sitter

(use-package treesit-auto
  :demand
  :custom
  (treesit-auto-install t)
  (major-mode-remap-alist '((markdown-ts-mode . markdown-mode)))
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

;;;; Compilation

(use-package compile
  :hook
  (compilation-filter . ansi-osc-compilation-filter)
  (compilation-filter . ansi-color-compilation-filter)
  :bind
  (:map code-map
        ("c" . compile)
        ("C" . recompile))
  :custom
  (compilation-ask-about-save nil)
  (compilation-scroll-output t))

;;;; Syntax checker

;; Have some mercy on me
(use-package flymake
  :ensure nil
  :hook (prog-mode)
  :bind
  (:map toggle-map ("f" . flymake-mode))
  (:map code-map   ("x" . consult-flymake))
  (:map flymake-mode-map
        ("M-n" . flymake-goto-next-error)
        ("M-p" . flymake-goto-prev-error))
  :custom
  (flymake-suppress-zero-counters t)
  (flymake-mode-line-counter-format
   '("  " flymake-mode-line-error-counter
     flymake-mode-line-warning-counter
     flymake-mode-line-note-counter))
  :config
  (remove-hook 'flymake-diagnostic-functions #'flymake-proc-legacy-flymake))

(use-package flymake-collection
  :hook (after-init . flymake-collection-hook-setup)
  :custom (flymake-collection-hook-ignore-modes
           '(eglot--managed-mode bash-ts-mode)))

;;;; Apheleia

(use-package apheleia
  :defer 1
  :hook (after-init . apheleia-global-mode)
  :bind
  (:map code-map ("f" . apheleia-format-buffer))
  (:map toggle-map ("a" . apheleia-mode))
  :config
  ;; Set custom formatting commands
  (dolist (formatter-cmd '((shfmt     . ("shfmt" "-i" "4" "-ci" "-kp" "-sr"))
                           (fourmolu  . ("fourmolu" "--indentation" "2" "--stdin-input-file"
                                         (or (buffer-file-name) (buffer-name))))))
    (add-to-list #'apheleia-formatters formatter-cmd))

  ;; Set custom formatters for modes
  (dolist (formatter-mode '((lisp-data-mode  . lisp-indent)
                            (scheme-mode     . lisp-indent)))
    (add-to-list #'apheleia-mode-alist formatter-mode))

  (defun +apheleia-inhibit-format-a (orig-fn &optional arg)
    "Make it so \\[save-buffer] with prefix arg inhibits reformatting."
    (let ((apheleia-mode (and apheleia-mode (member arg '(nil 1)))))
      (funcall orig-fn)
      apheleia-mode))

  (advice-add #'save-buffer :around #'+apheleia-inhibit-format-a))

;;;; Highlight TODO in programming modes

(defgroup +highlight-keywords nil
  "Highlight TODO and similar keywords in comments and strings."
  :group 'font-lock-extra-types)

(defcustom +highlight-keywords-faces
  '(("TODO"   . error)
    ("FIXME"  . error)
    ("HACK"   . warning)
    ("NOTE"   . warning))
  "Alist of keywords to highlight and their face."
  :group '+highlight-keywords
  :type '(alist :key-type string :value-type face))

(defun +highlight-keywords--keywords ()
  "Generate font-lock keywords from `+highlight-keywords-faces'."
  (let ((keywords (mapcar 'car +highlight-keywords-faces)))
    `((,(regexp-opt keywords 'words)
       (0 (when (nth 8 (syntax-ppss))
            (cdr (assoc (match-string 0) +highlight-keywords-faces)))
          prepend)))))

(define-minor-mode +highlight-keywords-mode
  "Highlight TODO and similar keywords in comments and strings."
  :group ' +highlight-keywords
  (if +highlight-keywords-mode
      (font-lock-add-keywords nil (+highlight-keywords--keywords) t)
    (font-lock-remove-keywords nil (+highlight-keywords--keywords)))
  (font-lock-flush))

(add-hook 'prog-mode-hook #'+highlight-keywords-mode)

;;; Programming languages

;;;; JavaScript

(use-package js
  :ensure nil
  :custom
  (js--prettify-symbols-alist nil)
  (js-indent-level 2)
  (js-switch-indent-offset 2))

;;;; HTML

(use-package web-mode
  :mode
  ("\\.html?\\'"  . web-mode)
  ("\\.hbs\\'"    . web-mode)
  ("\\.liquid\\'" . web-mode)
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-enable-auto-closing t)
  (web-mode-enable-auto-pairing nil))

;;;; CSS

(use-package css-mode
  :ensure nil
  :custom (css-indent-offset 2))

;;;; PureScript

(use-package purescript-mode
  :mode ("\\.purs\\'")
  :hook (purescript-mode . turn-on-purescript-indentation))

;;;; Haskell

(use-package haskell-mode
  :mode
  ("\\.hs\\'"    . haskell-mode)
  ("\\.cabal\\'" . haskell-cabal-mode)
  :hook
  (haskell-mode . interactive-haskell-mode)
  (haskell-mode . haskell-indentation-mode)
  :custom (haskell-process-auto-import-loaded-modules t))

;;;; Go

(use-package go-ts-mode
  :custom (go-ts-mode-indent-offset 2))

;;;; Scheme

(use-package geiser
  :bind (:map scheme-mode-map ("C-c M-j" . geiser))
  :custom (geiser-active-implementations '(guile)))

(use-package geiser-guile
  :custom (geiser-guile-binary "guile3.0"))

;;;; Common Lisp

(use-package sly
  :bind
  (:map sly-mode-map ("M-?" . nil))
  (:map lisp-mode-map ("C-c M-j" . sly))
  :custom
  (inferior-lisp-program (executable-find "sbcl"))
  (sly-symbol-completion-mode nil))

(use-package sly-mrepl
  :ensure nil
  :after sly
  :bind
  (:map sly-mrepl-mode-map
        ("M-r" . consult-history)
        ("M-s" . consult-history))
  :config
  (defun +sly-mrepl-toggle-a (&rest _args)
    "Switch to the last Lisp/sly-mrepl buffer."
    (interactive)
    (let ((buf (if (derived-mode-p 'sly-mrepl-mode)
                   (seq-find (lambda (b)
                               (with-current-buffer b
                                 (derived-mode-p 'lisp-mode)))
                             (buffer-list))
                 (sly-mrepl--find-create (sly-current-connection)))))
      (if buf
          (if-let* ((win (get-buffer-window buf)))
              (select-window win)
            (pop-to-buffer buf))
        (user-error "No %s buffer found"
                    (if (derived-mode-p 'sly-mrepl-mode) "Lisp" "sly-mrepl")))))

  (advice-add #'sly-mrepl :override #'+sly-mrepl-toggle-a))

;;;; Device Tree Source

(use-package dts-mode
  :mode
  ("\\.dtsi?\\'"  . dts-mode)
  ("\\.keymap\\'" . dts-mode))

;; Local Variables:
;; byte-compile-warnings: (not unresolved free-vars)
;; End:

;;; init.el ends here
