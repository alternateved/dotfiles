#!/bin/sh

_status() {
    pidf="$HOME/Videos/Screencasts/process.pid"
    if [ -f "$pidf" ]; then
        recording_name=$(basename "$(awk 'NR==2' "$pidf")")
        echo "{\"text\": \"\", \"tooltip\": \"Recording as $recording_name\"}"
    fi
}

_stop() {
    pidf="$HOME/Videos/Screencasts/process.pid"
    if [ -f "$pidf" ]; then
        pgrep -x wf-recorder > /dev/null && pkill -INT -x wf-recorder
        rm "$pidf"
    fi
}

case "$1" in
    status) _status ;;
    stop) _stop ;;
esac
