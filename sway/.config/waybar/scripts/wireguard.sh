#!/bin/sh

_status() {
    active_connection=$(nmcli connection show --active | grep wireguard | awk '{print $1}')
    if [ -n "$active_connection" ]; then
        ipv4_addresses=$(nmcli connection show "$active_connection" | awk '/ipv4.addresses:/ {print $2}')
        echo "{\"text\": \" $active_connection\", \"tooltip\": \"$ipv4_addresses\"}"
    fi
}

_toggle() {
    active_connection=$(nmcli connection show --active | grep wireguard | awk '{print $1}')
    if [ -n "$active_connection" ]; then
        nmcli connection down "$active_connection"
    fi
}

case "$1" in
    status) _status ;;
    toggle) _toggle ;;
esac
