#!/bin/sh
set -euo pipefail

_status() {
    dbus-monitor path='/org/freedesktop/Notifications',interface='org.freedesktop.DBus.Properties',member='PropertiesChanged' --profile |
        while read -r _; do
            paused="$(dunstctl is-paused)"
            if [ "$paused" == 'true' ]; then
                tooltip=""
                count="$(dunstctl count waiting)"
                if [ "$count" != '0' ]; then
                    tooltip="$count notification(s) awaiting"
                fi
                echo "{\"text\": \"\", \"tooltip\": \"$tooltip\"}"
            else
                echo "{\"text\": \"\", \"tooltip\": \"\"}"
            fi
        done
}

_toggle() {
    dunstctl set-paused toggle
}

case "$1" in
    status) _status ;;
    toggle) _toggle ;;
esac
