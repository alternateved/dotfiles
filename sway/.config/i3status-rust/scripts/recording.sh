#!/bin/sh

_status() {
    [ -f "$HOME/Videos/Screencasts/process.pid" ] && echo ""
}

_stop() {
    pidf="$HOME/Videos/Screencasts/process.pid"
    if [ -f "$pidf" ]; then
        pgrep -x wf-recorder > /dev/null && pkill -INT -x wf-recorder
        rm "$pidf"
    fi
}

case "$1" in
    status) _status ;;
    stop) _stop ;;
esac
