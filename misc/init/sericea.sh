#!/bin/bash

FEDORA_VERSION=$(rpm -E %fedora)
SCRIPT_DIR=$(cd "$(dirname "${0}")" &> /dev/null && pwd -P)

### Enable COPR
echo "Enabling COPR repositories..."
sudo wget https://copr.fedorainfracloud.org/coprs/alternateved/eza/repo/fedora-"$FEDORA_VERSION"/alternateved-eza-fedora-"$FEDORA_VERSION".repo -O /etc/yum.repos.d/_copr:alternateved:eza.repo
sudo wget https://copr.fedorainfracloud.org/coprs/alternateved/keyd/repo/fedora-"$FEDORA_VERSION"/alternateved-keyd-fedora-"$FEDORA_VERSION".repo -O /etc/yum.repos.d/_copr:alternateved:keyd.repo
sudo wget https://copr.fedorainfracloud.org/coprs/alternateved/cliphist/repo/fedora-"$FEDORA_VERSION"/alternateved-cliphist-fedora-"$FEDORA_VERSION".repo -O /etc/yum.repos.d/_copr:alternateved:cliphist.repo

### Setup Fedora tools
sudo rm -rf /etc/profile.d/colorls.*

echo "Enabling automatic updates..."
sudo sed -i 's/#AutomaticUpdatePolicy.*/AutomaticUpdatePolicy=stage/' /etc/rpm-ostreed.conf
sudo systemctl enable rpm-ostreed-automatic.timer
rpm-ostree cleanup -m

### Setup tools
echo "Installing tools..."
rpm-ostree override remove firefox firefox-langpacks
rpm-ostree install eza fd-find ripgrep mg stow keyd gvfs-fuse gvfs-mtp syncthing cliphist swappy wf-recorder

### Install flatpaks
"${SCRIPT_DIR}"/flatpak.sh

### Tweak GTK
gsettings set org.gnome.desktop.wm.preferences button-layout ''
