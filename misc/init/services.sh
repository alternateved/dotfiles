#!/bin/bash

_services() {
    local arg="$1"
    local rebase="$2"

    systemctl --user daemon-reload

    if [ "$rebase" != "true" ]; then
        systemctl --user "${arg}" --now flatpak-automatic.timer
        systemctl --user "${arg}" --now syncthing.service
        systemctl --user "${arg}" --now notmuch.timer
        systemctl --user "${arg}" --now emacs.service

        systemctl "${arg}" --now keyd.service
    fi

    if [ "$XDG_CURRENT_DESKTOP" = 'GNOME' ]; then
        systemctl --user "${arg}" --now theme-switcher.service
        systemctl --user "${arg}" --now gnome-keyring-daemon.service
    fi

    if [ "$XDG_CURRENT_DESKTOP" = 'sway' ]; then
        systemctl --user "${arg}" --now swayidle.service
        systemctl --user "${arg}" --now gnome-keyring-daemon.service
    fi
}

rebase_mode="false"
action=""

for arg in "$@"; do
    case "$arg" in
        --rebase | -r) rebase_mode="true" ;;
        enable | disable) action="$arg" ;;
    esac
done

if [ -z "$action" ]; then
    echo "Usage: $0 [--rebase|-r] {enable|disable}"
    exit 1
fi

_services "$action" "$rebase_mode"
