#!/bin/bash

echo "Tweaking Gnome..."
echo "MUTTER_CHECK_ALIVE_TIMEOUT=30" | sudo tee -a /etc/environment
echo "GNOME_SHELL_SLOWDOWN_FACTOR=0.8" | sudo tee -a /etc/environment

dconf write /org/gnome/settings-daemon/plugins/color/night-light-schedule-automatic "true"
dconf write /org/gnome/settings-daemon/plugins/color/night-light-enabled "true"
dconf write /org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-timeout "1800"
dconf write /org/gnome/desktop/session/idle-delay "600"
dconf write /org/gnome/desktop/wm/preferences/resize-with-right-button "true"
dconf write /org/gnome/desktop/wm/preferences/button-layout "'appmenu:close'"
dconf write /org/gnome/desktop/wm/preferences/focus-mode "'sloppy'"
dconf write /org/gnome/desktop/sound/allow-volume-above-100-percent "true"
dconf write /org/gnome/desktop/peripherals/touchpad/tap-to-click "true"
dconf write /org/gnome/desktop/privacy/remove-old-temp-files "true"
dconf write /org/gnome/desktop/input-sources/sources "[('xkb', 'pl')]"
dconf write /org/gnome/desktop/privacy/remove-old-trash-files "true"
dconf write /org/gnome/mutter/workspaces-only-on-primary "true"
dconf write /org/gnome/mutter/center-new-windows "true"
dconf write /org/gnome/mutter/experimental-features "['variable-refresh-rate', 'xwayland-native-scaling', 'scale-monitor-framebuffer']"

echo "Tweaking Gnome applications..."
dconf write /org/gtk/gtk4/settings/file-chooser/sort-directories-first "true"
dconf write /org/gtk/gtk4/settings/file-chooser/clock-format "'24h'"
dconf write /org/gnome/nautilus/list-view/use-tree-view "true"
dconf write /org/gnome/TextEditor/restore-session "false"
dconf write /org/gnome/GWeather4/temperature-unit "'centigrade'"

echo "Tweaking Gnome appearance..."
dconf write /org/gnome/desktop/interface/clock-format "'24h'"

dconf write /org/gnome/desktop/interface/font-antialiasing "'grayscale'"
dconf write /org/gnome/desktop/interface/monospace-font-name "'JetBrainsMono 11.5'"
dconf write /org/gnome/desktop/interface/document-font-name "'Inter 11'"
dconf write /org/gnome/desktop/interface/font-name "'Inter 11'"
dconf write /org/gnome/desktop/wm/preferences/titlebar-font "'Inter Bold 11'"
dconf write /org/gnome/desktop/interface/gtk-theme "'adw-gtk3'"
dconf write /org/gnome/desktop/background/picture-uri "'file:///usr/share/backgrounds/gnome/blobs-l.svg'"
dconf write /org/gnome/desktop/background/picture-uri-dark "'file:///usr/share/backgrounds/gnome/blobs-d.svg'"

echo "Tweaking Gnome extensions..."
dconf write /org/gnome/shell/extensions/caffeine/show-notifications "false"
dconf write /org/gnome/shell/extensions/auto-move-windows/application-list "['com.slack.Slack.desktop:1', 'org.chromium.Chromium.desktop:1', 'org.signal.Signal.desktop:3', 'com.discordapp.Discord.desktop:3', 'org.gnome.Fractal.desktop:3', 'org.gnome.Boxes.desktop:4', 'com.mastermindzh.tidal-hifi.desktop:4', 'com.calibre_ebook.calibre.desktop:4']"

echo "Tweaking Gnome keybindings..."
dconf write /org/gnome/shell/keybindings/toggle-overview "['<Super>space']"
dconf write /org/gnome/shell/keybindings/toggle-message-tray "['<Super>y']"
dconf write /org/gnome/shell/keybindings/focus-active-notification "['<Super>u']"

dconf write /org/gnome/mutter/keybindings/switch-monitor "['XF86Display']"
dconf write /org/gnome/mutter/keybindings/toggle-tiled-left "['<Super>m','<Super>Left']"
dconf write /org/gnome/mutter/keybindings/toggle-tiled-right "['<Super>i','<Super>Right']"

dconf write /org/gnome/desktop/wm/keybindings/maximize "['<Super>e','<Super>Up']"
dconf write /org/gnome/desktop/wm/keybindings/unmaximize "['<Super>n','<Super>Down','<Alt>F5']"
dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-1 "['<Super>k','<Super>Home']"
dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-left "['<Super>h','<Super>Page_Down','<Shift><Super>Tab']"
dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-right "['<Super>comma','<Super>Page_Up','<Super>Tab']"
dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-last "['<Super>period','<Super>End']"
dconf write /org/gnome/desktop/wm/keybindings/move-to-workspace-1 "['<Shift><Super>k','<Shift><Super>Home']"
dconf write /org/gnome/desktop/wm/keybindings/move-to-workspace-left "['<Shift><Super>h','<Shift><Super>Page_Down']"
dconf write /org/gnome/desktop/wm/keybindings/move-to-workspace-right "['<Shift><Super>comma','<Shift><Super>Page_Up']"
dconf write /org/gnome/desktop/wm/keybindings/move-to-workspace-last "['<Super><Shift>period','<Super><Shift>End']"
dconf write /org/gnome/desktop/wm/keybindings/move-to-monitor-left "['<Shift><Super>m','<Shift><Super>Left']"
dconf write /org/gnome/desktop/wm/keybindings/move-to-monitor-right "['<Shift><Super>i','<Shift><Super>Right']"

dconf write /org/gnome/desktop/wm/keybindings/switch-input-source "['']"
dconf write /org/gnome/desktop/wm/keybindings/switch-input-source-backward "['']"
dconf write /org/gnome/desktop/wm/keybindings/close "['<Super>c','<Alt>F4']"
dconf write /org/gnome/desktop/wm/keybindings/cycle-windows "['<Alt>Tab']"
dconf write /org/gnome/desktop/wm/keybindings/cycle-windows-backward "['<Shift><Alt>Tab']"
dconf write /org/gnome/desktop/wm/keybindings/switch-group "['<Super>w']"
dconf write /org/gnome/desktop/wm/keybindings/switch-group-backward "['<Shift><Super>w']"
dconf write /org/gnome/desktop/wm/keybindings/switch-applications "['<Alt>Escape']"
dconf write /org/gnome/desktop/wm/keybindings/switch-applications-backward "['<Shift><Alt>Escape']"
dconf write /org/gnome/desktop/wm/keybindings/minimize "@as []"
dconf write /org/gnome/desktop/wm/keybindings/activate-window-menu "['<Super>Menu']"
dconf write /org/gnome/desktop/wm/keybindings/toggle-fullscreen "['<Super>f']"
dconf write /org/gnome/desktop/wm/keybindings/panel-run-dialog "['<Shift><Super>r','<Alt>F2']"

dconf write /org/gnome/settings-daemon/plugins/media-keys/www "['<Shift><Super>b']"
dconf write /org/gnome/settings-daemon/plugins/media-keys/home "['<Shift><Super>f']"
dconf write /org/gnome/settings-daemon/plugins/media-keys/control-center "['<Shift><Super>s']"
dconf write /org/gnome/settings-daemon/plugins/media-keys/logout "['<Shift><Super>q']"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/']"

dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/binding "'<Super>Return'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/command "'ghostty'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/name "'ghostty'"

dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/binding "'<Shift><Super>Return'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/name "'emacsclient'"

if command -v rpm-ostree > /dev/null; then
    dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/command "'toolbox run emacsclient -c -a \'\''"
else
    dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/command "'emacsclient -c -a \'\''"
fi

dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/binding "'<Super>F5'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/command "'/home/alternateved/.scripts/toggle-theme gnome-minimal'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/name "'toggle-theme'"

dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/binding "'<Super>F4'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/command "'/home/alternateved/.scripts/toggle-sound-output'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/name "'toggle-sound-output'"

dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/binding "'<Super>F8'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/command "'/home/alternateved/.scripts/get-devices-status'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/name "'get-devices-status'"

dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/binding "'<Shift><Control>backslash'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/command "'/home/alternateved/.scripts/toggle-dnd'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/name "'toggle-dnd'"

echo "Tweaking other keybindings..."
dconf write /desktop/ibus/panel/emoji/hotkey "['']"
dconf write /desktop/ibus/panel/emoji/unicode-hotkey "['']"
