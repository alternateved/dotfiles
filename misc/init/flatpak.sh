#!/bin/bash

_log() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $*" >&2
}

_install_flatpaks() {
    local apps=("$@")
    for app in "${apps[@]}"; do
        if ! flatpak install --noninteractive flathub "$app"; then
            _log "Failed to install $app"
        fi
    done
}

_log "Enabling Flathub"
flatpak remote-delete flathub --force
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

if command -v dnf > /dev/null || command -v rpm-ostree > /dev/null; then
    _log "Clearing previously installed flatpaks..."
    flatpak remote-delete fedora --force
    flatpak remote-delete fedora-testing --force
    flatpak remove --system --noninteractive --all
fi

if command -v rpm-ostree > /dev/null; then
    if [ "$XDG_CURRENT_DESKTOP" = 'GNOME' ]; then
        _log "Installing Gnome Core flatpaks..."
        gnome_core_apps=(
            org.gnome.baobab
            org.gnome.Calculator
            org.gnome.Calendar
            org.gnome.Characters
            org.gnome.clocks
            org.gnome.Contacts
            org.gnome.Papers
            org.gnome.font-viewer
            org.gnome.Loupe
            org.gnome.Logs
            org.gnome.Maps
            org.gnome.NautilusPreviewer
            org.gnome.Snapshot
            org.gnome.Showtime
            org.gnome.Decibels
            org.gnome.TextEditor
            org.gnome.Weather
        )
        _install_flatpaks "${gnome_core_apps[@]}"

        _log "Installing Gnome Utility flatpaks..."
        gnome_utility_apps=(
            org.gnome.Boxes
            page.tesk.Refine
            com.github.tchx84.Flatseal
            com.mattjakeman.ExtensionManager
            org.gtk.Gtk3theme.adw-gtk3
            org.gtk.Gtk3theme.adw-gtk3-dark
        )
        _install_flatpaks "${gnome_utility_apps[@]}"

        _log "Installing other Gnome flatpaks..."
        gnome_misc_apps=(
            io.bassi.Amberol
            com.github.PintaProject.Pinta
            com.github.johnfactotum.Foliate
        )
        _install_flatpaks "${gnome_misc_apps[@]}"
    fi

    if [ "$XDG_CURRENT_DESKTOP" = 'sway' ]; then
        _log "Installing utility flatpaks..."
        sway_apps=(
            io.mpv.Mpv
            org.gnome.Boxes
            com.github.tchx84.Flatseal
        )
        _install_flatpaks "${sway_apps[@]}"
    fi

    if [ "$XDG_CURRENT_DESKTOP" = 'KDE' ]; then
        kde_apps=(
            org.kde.gwenview
            org.kde.kcalc
            org.kde.krdc
            org.kde.okular
            org.kde.elisa
            org.kde.haruna
            org.gtk.Gtk3theme.Breeze
        )
        _install_flatpaks "${kde_apps[@]}"
    fi

fi

_log "Installing browser flatpaks..."
browser_apps=(
    com.obsproject.Studio.Plugin.Gstreamer
    com.obsproject.Studio.Plugin.GStreamerVaapi
    org.freedesktop.Platform.ffmpeg-full/x86_64/24.08
    org.mozilla.firefox
    org.chromium.Chromium
)
_install_flatpaks "${browser_apps[@]}"

_log "Installing LibreOffice flatpak..."
_install_flatpaks org.libreoffice.LibreOffice

_log "Installing other flatpaks..."
other_apps=(
    com.mastermindzh.tidal-hifi
    com.calibre_ebook.calibre
    com.valvesoftware.Steam
    org.signal.Signal
)
_install_flatpaks "${other_apps[@]}"
