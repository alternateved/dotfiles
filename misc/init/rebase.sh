#!/bin/bash

FEDORA_VERSION=$(rpm -E %fedora)
SCRIPT_DIR=$(cd "$(dirname "${0}")" &> /dev/null && pwd -P)

_rebase() {
    "${SCRIPT_DIR}"/services.sh disable --rebase
    rpm-ostree reset
    rpm-ostree rebase fedora:fedora/"$FEDORA_VERSION"/x86_64/"$1"

    sudo rm /etc/yum.repos.d/_copr*
    sudo wget https://copr.fedorainfracloud.org/coprs/alternateved/eza/repo/fedora-"$FEDORA_VERSION"/alternateved-eza-fedora-"$FEDORA_VERSION".repo -O /etc/yum.repos.d/_copr:alternateved:eza.repo
    sudo wget https://copr.fedorainfracloud.org/coprs/alternateved/keyd/repo/fedora-"$FEDORA_VERSION"/alternateved-keyd-fedora-"$FEDORA_VERSION".repo -O /etc/yum.repos.d/_copr:alternateved:keyd.repo
}

case "$1" in
    silverblue)
        _rebase "$1"
        sudo wget https://copr.fedorainfracloud.org/coprs/pgdev/ghostty/repo/fedora-"$FEDORA_VERSION"/pgdev-ghostty-fedora-"$FEDORA_VERSION".repo -O /etc/yum.repos.d/_copr:pgdev:ghostty.repo

        rpm-ostree override remove firefox firefox-langpacks
        rpm-ostree install eza fd-find ripgrep mg keyd syncthing ghostty-git

        dconf write /org/gnome/desktop/wm/preferences/button-layout "'appmenu:close'"
        XDG_CURRENT_DESKTOP=GNOME "${SCRIPT_DIR}"/flatpak.sh
        ;;
    sericea)
        _rebase "$1"
        sudo wget https://copr.fedorainfracloud.org/coprs/alternateved/cliphist/repo/fedora-"$FEDORA_VERSION"/alternateved-cliphist-fedora-"$FEDORA_VERSION".repo -O /etc/yum.repos.d/_copr:alternateved:cliphist.repo

        rpm-ostree override remove firefox firefox-langpacks
        rpm-ostree install eza fd-find ripgrep mg keyd gvfs-fuse gvfs-mtp syncthing cliphist swappy wf-recorder

        gsettings set org.gnome.desktop.wm.preferences button-layout ''
        XDG_CURRENT_DESKTOP=sway "${SCRIPT_DIR}"/flatpak.sh
        ;;
    kinoite)
        _rebase "$1"
        rpm-ostree override remove firefox firefox-langpacks
        rpm-ostree install eza fd-find ripgrep mg jmtpfs keyd syncthing
        XDG_CURRENT_DESKTOP=KDE "${SCRIPT_DIR}"/flatpak.sh
        ;;
    *)
        echo "Usage: $0 {silverblue|sericea|kinoite}"
        exit 1
        ;;
esac
